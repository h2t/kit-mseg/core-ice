<!--
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
-->

<project name="mseginterfacesjava" default="all" xmlns:ivy="antlib:org.apache.ivy.ant">

    <property name="base.dir" value="." />
    <property name="ii_version" value="" />
    <property name="pkg.prefix" value="" />
    <property name="pkg.dependencies" value="" />
    <property name="pkg.maintainer" value="" />
    <property name="pkg.description_pre" value="" />
    
    <property name="pkg.description" value="${pkg.description_pre} Java Ice Interfaces" />

    <property name="lib.java.dir" value="/usr/share/java" />
    <property name="output.dir" value="${base.dir}/build" />

    <property name="classes.dir" value="${output.dir}/classes" />
    <property name="lib.dir" value="${output.dir}/lib" />
    <property name="src.dir" value="${base.dir}/src" />

    <path id="classpath">
        <fileset dir="${lib.java.dir}">
            <include name="Ice.jar" />
            <include name="IceGrid.jar" />
            <include name="Glacier2.jar" />
        </fileset>
    </path>

    <target name="install-jdeb">
        <ivy:retrieve organisation="org.vafer" module="jdeb" revision="1.6" inline="true" type="maven-plugin" pattern="${user.home}/.ant/lib/[artifact].[ext]"/>
    </target>

    <target name="mseginterfacesjava-compile">
        <mkdir dir="${classes.dir}" />
        <javac srcdir="${src.dir}" destdir="${classes.dir}" classpathref="classpath" />
    </target>

    <target name="mseginterfacesjava-jar" depends="mseginterfacesjava-compile">
        <pathconvert property="manifest.classpath.filenames" pathsep=" ">
            <path refid="classpath"/>
            <mapper>
                <chainedmapper>
                    <flattenmapper />
                    <globmapper from="*.jar" to="*.jar"/>
                </chainedmapper>
            </mapper>
        </pathconvert>

        <jar jarfile="${lib.dir}/${ant.project.name}.jar" basedir="${classes.dir}" filesetmanifest="skip">
            <include name="**/*.class" />
            <manifest>
                <attribute name="Created-By" value="MSeg" />
                <attribute name="Class-Path" value="${manifest.classpath.filenames}" />
            </manifest>
        </jar>
    </target>

    <target name="mseginterfacesjava-package" depends="install-jdeb">
        <taskdef name="deb" classname="org.vafer.jdeb.ant.DebAntTask" />

        <copy todir="${output.dir}/deb/control">
            <fileset dir="${base.dir}/deb" />
            <filterset begintoken="[[" endtoken="]]">
                <filter token="version" value="${ii_version}" />
                <filter token="description" value="${pkg.description}" />
                <filter token="dependencies" value="${pkg.dependencies}" />
                <filter token="maintainer" value="${pkg.maintainer}" />
                <filter token="packagename" value="${pkg.prefix}-java" />
            </filterset>
        </copy>

        <deb destfile="${output.dir}/${pkg.prefix}-java_${ii_version}_amd64.deb" control="${output.dir}/deb/control" verbose="true">
            <data src="${output.dir}/lib/${ant.project.name}.jar" type="file">
                <mapper type="perm" prefix="/usr/share/java" />
            </data>
        </deb>
    </target>

    <target name="all" depends="mseginterfacesjava-jar" />

</project>
