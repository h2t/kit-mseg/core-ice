# Allow number of jobs to be set
THREADS ?= 1

# Ice Interfaces version
ver_major = 1
ver_minor = 0
ver_patch = 0

# Package_info
pkg_prefix = mseg-interfaces
pkg_section = devel
pkg_priority = optional
pkg_maintainer = Christian R. G. Dreher <christian.dreher@student.kit.edu>
pkg_architecture = amd64
pkg_description_pre = MSeg Motion Segmentation Algorithm Evaluation Framework ::

# Dependencies
pkg_deps_java = openjdk-7-jdk
pkg_deps_python = python-zeroc-ice

# Paths
etc = ./etc
build_cpp = buildcpp
build_java = buildjava
build_py = buildpython
deb_dist = ./deb-dist

# Lists deb dependencies
.depsdeb:
	@echo ant
	@echo cmake
	@echo ivy
	@echo openjdk-7-jdk
	@echo zeroc-ice35

.bashrc:
	@printf "# <CORE-ICE>\n"
	@printf "export MSEG_CORE_ICE_DIR=\"$(realpath $(dir $(lastword $(MAKEFILE_LIST))))\"\n"
	@printf "export CLASSPATH=\"\$${CLASSPATH}:\$${MSEG_CORE_ICE_DIR}/${build_java}/build/lib/mseginterfacesjava.jar\"\n"
	@printf "# </CORE-iCE>\n\n"

# Delete build and dist directories
clean:
	@# Clean up installed files from Python
	@xargs rm 2> /dev/null < ./${build_py}/installed-files.log || true
	@rm -r ./${build_cpp} 2> /dev/null || true
	@rm -r ./${build_java} 2> /dev/null || true
	@rm -r ./${build_py} 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true

# Generates the C++ Ice interface and builds it
interfacescpp:
	# Create basic directory structure
	@mkdir -p ./${build_cpp}/build 2> /dev/null || true
	@mkdir -p ./${build_cpp}/source/MSeg/interface 2> /dev/null || true
	# Create the slice files
	slice2cpp --underscore -I./source -I./${build_cpp}/source/MSeg/interface ./source/MSeg/interface/*.ice --include-dir MSeg/interface --output-dir ./${build_cpp}/source/MSeg/interface
	# Copy needed build files
	@cp ${etc}/cpp/CMakeLists.txt ./${build_cpp}/CMakeLists.txt
	@cp ${etc}/cpp/mseginterfacescppConfig.cmake.in ./${build_cpp}/mseginterfacescppConfig.cmake.in
	@cp ${etc}/cpp/mseginterfacescppConfigVersion.cmake.in ./${build_cpp}/mseginterfacescppConfigVersion.cmake.in
	@cp ${etc}/cpp/mseginterfacescppBuildTreeSettings.cmake.in ./${build_cpp}/mseginterfacescppBuildTreeSettings.cmake.in
	# Run cmake and compile the interfaces
	cmake -DPKG_PREFIX="${pkg_prefix}" -DPKG_DESCRIPTION_PRE="${pkg_description_pre}" -DPKG_MAINTAINER="${pkg_maintainer}" -DII_VERSION_MAJOR=${ver_major} -DII_VERSION_MINOR=${ver_minor} -DII_VERSION_PATCH=${ver_patch} -B./${build_cpp}/build -H./${build_cpp}
	make --no-print-directory --directory=./${build_cpp}/build -j${THREADS}

# Generates the Java Ice interface and builds it
interfacesjava:
	# Create basic directory structure
	@mkdir -p ./${build_java}/build/lib 2> /dev/null || true
	@mkdir -p ./${build_java}/src 2> /dev/null || true
	@mkdir -p ./${build_java}/deb 2> /dev/null || true
	# Create the slice files
	slice2java --underscore -I./source ./source/MSeg/interface/*.ice --output-dir ./${build_java}/src
	# Copy needed build files
	@cp ${etc}/java/build.xml ./${build_java}/build.xml
	@cp ${etc}/java/deb/control ./${build_java}/deb/control
	# Run ant and compile the interfaces
	ant -lib "/usr/share/java/ivy.jar" -buildfile ./${build_java}/build.xml -Dbase.dir="." -Dpkg.prefix="${pkg_prefix}" -Dpkg.description_pre="${pkg_description_pre}" -Dpkg.maintainer="${pkg_maintainer}" -Dpkg.dependencies="${pkg_deps_java}" -Dii_version="${ver_major}.${ver_minor}.${ver_patch}" mseginterfacesjava-jar

# Generates the Python interface
interfacespython:
	# Create basic directory structure
	@mkdir -p ./${build_py}/mseg 2> /dev/null || true
	# Create the slice files
	slice2py --underscore -I./source --all ./source/MSeg/interface/*.ice --output-dir ./${build_py}/mseg
	# Fix strange behaviour of slice2py regarding modules
	@mv ./${build_py}/mseg/mseg/__init__.py ./${build_py}/mseg/__init__.py
	@rm -r ./${build_py}/mseg/mseg
	# Copy the needed build files
	@cp ${etc}/python/setup.py ./${build_py}/setup.py
	# Install interfaces
	cd ./${build_py} && II_VERSION="${ver_major}.${ver_minor}.${ver_patch}" python ./setup.py install --user --record ./installed-files.log

# Builds a Debian package for the C++ Ice interface
interfacescpp-package: interfacescpp
	make --no-print-directory --directory=./${build_cpp}/build package

# Builds a Debian package for the Java Ice interface
interfacesjava-package: interfacesjava
	ant -lib "/usr/share/java/ivy.jar" -buildfile ./${build_java}/build.xml -Dbase.dir="." -Dpkg.prefix="${pkg_prefix}" -Dpkg.description_pre="${pkg_description_pre}" -Dpkg.maintainer="${pkg_maintainer}" -Dpkg.dependencies="${pkg_deps_java}" -Dii_version="${ver_major}.${ver_minor}.${ver_patch}" mseginterfacesjava-package

# Builds a Debian package for the Python Ice interface
interfacespython-package: interfacespython
	@echo "[DEFAULT]" > ./${build_py}/stdeb.cfg
	@echo "Package: ${pkg_prefix}-python" >> ./${build_py}/stdeb.cfg
	@echo "Section: ${pkg_section}" >> ./${build_py}/stdeb.cfg
	@echo "Priority: ${pkg_priority}" >> ./${build_py}/stdeb.cfg
	@echo "Maintainer: ${pkg_maintainer}" >> ./${build_py}/stdeb.cfg
	@echo "Depends: ${pkg_deps_python}" >> ./${build_py}/stdeb.cfg
	@echo "Architecture: ${pkg_architecture}" >> ./${build_py}/stdeb.cfg
	@echo "Description: ${pkg_description_pre} Python Ice Interfaces" >> ./${build_py}/stdeb.cfg
	cd ./${build_py} && II_VERSION="${ver_major}.${ver_minor}.${ver_patch}" python ./setup.py --command-packages=stdeb.command bdist_deb

# Build all interfaces
install: interfacescpp interfacesjava interfacespython

# Build all packages and move them to ${deb_dist}
package: interfacescpp-package interfacesjava-package interfacespython-package
	@mkdir ${deb_dist} 2> /dev/null || true
	@mv ./${build_cpp}/build/*.deb ${deb_dist}/
	@mv ./${build_java}/build/*.deb ${deb_dist}/
	@mv ./${build_py}/deb_dist/*.deb ${deb_dist}/
