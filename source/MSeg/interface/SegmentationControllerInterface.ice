/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::interface::SegmentationControllerInterface
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MSeg/interface/DataTypes.ice>
#include <MSeg/interface/exceptions.ice>
#include <MSeg/interface/Topics.ice>

module mseg
{
    interface SegmentationControllerInterface extends SegmentationProgressIndividualTopic
    {
        void setDataset(MotionRecordingDataset dataset);
        VectorAlgorithmParameter getAlgorithmParameters(string algorithmName)
            throws AlgorithmOfflineException;
        void setAlgorithmParameters(string algorithmName, VectorAlgorithmParameter params)
            throws AlgorithmOfflineException;
        SegmentationResultDataset segmentDataset(string algorithmName)
            throws MSegException, AlgorithmOfflineException;
        VectorString querySegmentationAlgorithms();
    };
};
