/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::interface::DataTypes
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

module mseg
{
    sequence<int> VectorInt;
    sequence<float> VectorFloat;
    sequence<string> VectorString;

    enum VarTypes
    {
        BoolType,
        IntType,
        FloatType,
        StringType,
        JsonType
    };

    enum Granularity
    {
        Rough = 3,
        Medium = 2,
        Fine = 1
    };

    struct AlgorithmParameter
    {
        string name = "";
        string description = "";
        VarTypes type;
        string defaultValue = "";
        string value = "";

        int intmin = 0;
        int intmax = 0;
        float floatmin = 0.0f;
        float floatmax = 0.0f;
        int decimals = 0;
    };

    sequence<AlgorithmParameter> VectorAlgorithmParameter;

    struct MotionRecording
    {
        string filename = "";
        string path = "";
        int frameCount = -1;
    };

    sequence<MotionRecording> VectorMotionRecording;

    struct MotionRecordingDataset
    {
        VectorMotionRecording recordings;
        string datasetName = "Custom";
    };

    struct SegmentationResult
    {
        MotionRecording recording;
        VectorInt keyFrames;
    };

    sequence<SegmentationResult> VectorSegmentationResult;

    struct SegmentationResultDataset
    {
        VectorSegmentationResult results;
        string datasetName = "Custom";
        string algorithmName = "";
        VectorAlgorithmParameter parameters;
        bool trainingRequired = false;
        bool onlineFetched = false;
    };

    class EvaluationResult;

    sequence<EvaluationResult> VectorEvaluationResult;

    class EvaluationResult
    {
        string name = "";
        bool isGroup = false;
        bool isGroupExpanded = false;
        bool collapseGroup = false;
        string groupName = "";
        string groupAttributeName = "";
        string groupAttributeValue = "";
        double score = 0.0f;
        bool isScoreInt = true;
        VectorEvaluationResult subResults;
    };

    class FormattedEvaluationResult;

    sequence<FormattedEvaluationResult> VectorFormattedEvaluationResult;

    class FormattedEvaluationResult
    {
        string name = "";
        string value = "";
        VarTypes type = IntType;
        bool isGroup = false;
        bool isGroupExpanded = false;
        bool collapseGroup = false;
        string groupName = "";
        string groupAttributeName = "";
        string groupAttributeValue = "";
        VectorFormattedEvaluationResult subResults;
    };
};
